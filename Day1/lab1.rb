class ComplexNumber
  attr_accessor :imaginary,:real
  @@stats = {additions:  0 , multiplications: 0}
  def initialize(real=0, imaginary=0)
    @real = real
    @imaginary = imaginary
  end
  def +(complex_number)
    temp_complex_number = ComplexNumber.new
    temp_complex_number.real = @real + complex_number.real
    temp_complex_number.imaginary = @imaginary + complex_number.imaginary
    @@stats[:additions] += 1
    temp_complex_number
  end
  def -(complex_number)
    temp_complex_number = ComplexNumber.new
    temp_complex_number.real = @real - complex_number.real
    temp_complex_number.imaginary = @imaginary - complex_number.imaginary
    temp_complex_number
  end
  def *(complex_number)
    temp_complex_number = ComplexNumber.new
    temp_complex_number.real = ((@real * complex_number.real)-(@imaginary*complex_number.imaginary))
    temp_complex_number.imaginary = ((@real * complex_number.imaginary)+(@imaginary*complex_number.real))
    @@stats[:multiplications] += 1
    temp_complex_number
  end
  def printComplexNumber(complex_number)
    puts "realPart = #{complex_number.real}, imaginaryPart = #{complex_number.imaginary}"
  end
  def self.bulkAdd(complex_numbers)
    total_complex_numbers_sum = ComplexNumber.new
    complex_numbers.each do |complex_number|
      total_complex_numbers_sum += complex_number
    end
    @@stats[:additions] += 1
    total_complex_numbers_sum
  end
  def self.bulkMultiplication(complex_numbers)
    total_complex_numbers_sum = ComplexNumber.new(1,1)
    complex_numbers.each do |complex_number|
      total_complex_numbers_sum *= complex_number
    end
    @@stats[:multiplications] +=1
    total_complex_numbers_sum
  end
  def self.get_stats
    puts "number of Additions = #{@@stats[:additions]} , number od Multiplications #{@@stats[:multiplications]}"
  end
end


complex_number1 = ComplexNumber.new(1,2)
complex_number2 = ComplexNumber.new(2,3)
complex_number3 = ComplexNumber.new
array = [complex_number1,complex_number2]
complex_number3 = complex_number1 + complex_number2
puts complex_number3.real
