def one_two_three
  yield 1
  yield 2
  yield 3
end


one_two_three {|number| puts number * 10}

lam = lambda {|x| puts x}

puts lam

lam.call(1)

$global_var = 10
class Class1
  def print_global
    puts "gloval_var is #{$global_var}"
  end
end


class Class2
  def print_global
    puts "gloval_var is #{$global_var}"
    $global_var += 1
  end
end


# Class1.new.print_global
Class2.new.print_global
Class1.new.print_global


class Class3
  def initialize(id, name, address)
    @id = id
    @name = name
    @address = address
  end
  attr_accessor :id,:name,:address
  # def id
  #   @id
  #   name
  #   @name
  # end
  # def name
  #   @name
  # end

end

class3 = Class3.new(1,"ahmed","6546")
class3.name = "shf3y"
puts class3.name
